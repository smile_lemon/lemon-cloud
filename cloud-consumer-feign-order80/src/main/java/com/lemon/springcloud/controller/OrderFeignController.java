package com.lemon.springcloud.controller;

import com.lemon.springcloud.entities.CommonResult;
import com.lemon.springcloud.entities.Payment;
import com.lemon.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @auther 柠檬
 * @create 2022-04-09  15:51
 **/

@RestController
@Slf4j
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping(value="/consumer/payment/get/{id}")
    public CommonResult<Payment> get(@PathVariable("id") long id){
        log.info("payment id="+id);
        return  paymentFeignService.getPaymentById(id);
    }
}
