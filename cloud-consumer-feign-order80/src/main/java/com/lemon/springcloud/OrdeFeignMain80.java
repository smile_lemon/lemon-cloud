package com.lemon.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @auther 柠檬
 * @create 2022-04-09  15:33
 **/

@SpringBootApplication
@EnableFeignClients
public class OrdeFeignMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrdeFeignMain80.class,args);
    }
}
