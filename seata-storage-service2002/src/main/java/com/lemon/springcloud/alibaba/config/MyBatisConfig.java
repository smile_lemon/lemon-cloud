package com.lemon.springcloud.alibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @auther 柠檬
 * @create 2022-04-20  17:01
 **/

@Configuration
@MapperScan({"com.lemon.springcloud.alibaba.dao"})
public class MyBatisConfig {

}
