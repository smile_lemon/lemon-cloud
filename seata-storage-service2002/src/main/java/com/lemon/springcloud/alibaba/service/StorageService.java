package com.lemon.springcloud.alibaba.service;

/**
 * @auther 柠檬
 * @create 2022-04-20  17:08
 **/


public interface StorageService {

    void decrease(Long productId, Integer count);
}
