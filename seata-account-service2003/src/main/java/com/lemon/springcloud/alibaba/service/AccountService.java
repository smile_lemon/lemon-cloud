package com.lemon.springcloud.alibaba.service;

import java.math.BigDecimal;

/**
 * @auther 柠檬
 * @create 2022-04-20  18:01
 **/

public interface AccountService {
    void decrease(Long userId, BigDecimal money);
}
