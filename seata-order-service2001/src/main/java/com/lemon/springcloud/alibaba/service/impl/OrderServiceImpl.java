package com.lemon.springcloud.alibaba.service.impl;

import com.lemon.springcloud.alibaba.dao.OrderDao;
import com.lemon.springcloud.alibaba.domain.Order;
import com.lemon.springcloud.alibaba.service.AccountService;
import com.lemon.springcloud.alibaba.service.OrderService;
import com.lemon.springcloud.alibaba.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @auther 柠檬
 * @create 2022-04-20  16:59
 **/

@Service
@Slf4j
public class OrderServiceImpl implements OrderService
{
    @Resource
    private OrderDao orderDao;
    @Resource
    private AccountService accountService;
    @Resource
    private StorageService storageService;

    /**
     * 创建订单->调用库存服务扣减库存->调用账户服务扣减账户余额->修改订单状态
     * 简单说：下订单->扣库存->减余额->改状态
     */
    @Override
    @GlobalTransactional(name = "fsp-create-order",rollbackFor = Exception.class)
    public void create(Order order) {
        log.info("----->开始新建订单");
        log.info("----->下订单");
        orderDao.create(order);
        log.info("----->扣库存");
        storageService.decrease(order.getProductId(),order.getCount());
        log.info("----->减余额");
        accountService.decrease(order.getUserId(),order.getMoney());
        log.info("----->改状态");
        orderDao.update(order.getUserId(),0);
        log.info("----->完成订单");
    }
}
