package com.lemon.springcloud.alibaba.dao;

import com.lemon.springcloud.alibaba.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @auther 柠檬
 * @create 2022-04-20  16:50
 **/

@Mapper
public interface OrderDao {

    int create(Order order);
    //2 修改订单状态，从零改为1
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}
