package com.lemon.springcloud.alibaba.service;

import com.lemon.springcloud.alibaba.domain.Order;

/**
 * @auther 柠檬
 * @create 2022-04-20  16:56
 **/


public interface OrderService
{
    void create(Order order);

}