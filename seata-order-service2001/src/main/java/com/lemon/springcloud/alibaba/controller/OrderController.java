package com.lemon.springcloud.alibaba.controller;

import com.lemon.springcloud.alibaba.domain.CommonResult;
import com.lemon.springcloud.alibaba.domain.Order;
import com.lemon.springcloud.alibaba.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @auther 柠檬
 * @create 2022-04-20  16:51
 **/

@RestController
public class OrderController
{
    @Resource
    private OrderService orderService;


    @GetMapping("/order/create")
    public CommonResult create(Order order)
    {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功");
    }
}
