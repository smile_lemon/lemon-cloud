# 学习spring cloud

## 支付功能  cloud-provider-payment8001

## 消费功能 

### 配置 workspace.xml
```
<component name="RunDashboard">
  <option name="configurationTypes">
    <set>
      <option value="SpringBootApplicationConfigurationType" />
    </set>
  </option>
  <option name="ruleStates">
    <list>
      <RuleState>
        <option name="name" value="ConfigurationTypeDashboardGroupingRule" />
      </RuleState>
      <RuleState>
        <option name="name" value="StatusDashboardGroupingRule" />
      </RuleState>
    </list>
  </option>
  <option name="contentProportion" value="0.20013662" />
</component>

```
调出多个module 运行窗口

## 第二阶段 初级
### Eureka  
  Eureka 包含两个组件：Eureka
  Server 和 Eureka Client
  单机Euraka
  http://localhost:7001/
  
### Eureka 集群
	互相注册， 相互守望
	
	http://eureka7001.com:7001/
    http://eureka7002.com:7002/
  
  
 属于CAP 中的AP
 
 ### zookeeper

 centos7 安装 zookeeper 的方法
 https://blog.csdn.net/laogadai/article/details/93797016

  查看是否访问成功
  ```
  #链接 zookeeper
  [root@lemon bin]# ./zkCli.sh
  
  # 查询根节点
  [zk: localhost:2181(CONNECTED) 1] ls /
  [services, zookeeper]

  [zk: localhost:2181(CONNECTED) 1] ls /zookeeper

  [zk: localhost:2181(CONNECTED) 1] ls /zookeeper	

  [zk: localhost:2181(CONNECTED) 4] get  /services/cloud-provider-payment/ea148cea-93f4-48d9-9f64-4433844d79ec
  
  {"name":"cloud-provider-payment","id":"ea148cea-93f4-48d9-9f64-4433844d79ec","address":"LAPTOP-9G0GBU07","port":8004,"sslPort":null,"payload":{"@class":"org.springframework.cloud.zookeeper.discovery.ZookeeperInstance","id":"application-1","name":"cloud-provider-payment","metadata":{}},"registrationTimeUTC":1649144248045,"serviceType":"DYNAMIC","uriSpec":{"parts":[{"value":"scheme","variable":true},{"value":"://","variable":false},{"value":"address","variable":true},{"value":":","variable":false},{"value":"port","variable":true}]}}


  ```
  服务属于监时
   属于CAP 中的cP
		
### consul 
属于CAP 中的cP
官网：https://www.consul.io/downloads
windows 安装：https://www.cnblogs.com/aixinge/p/9118337.html
访问路径：	http://localhost:8500/ui/dc1/services

支付功能
http://localhost:8006/payment/consul
消费
http://localhost/consumer/payment/consul


### Ribbon


http://localhost/consumer/payment/getEntity/1

### openFegin


## 第三阶段 中级

###  10.Hystrix

    https://github.com/Netflix/Hystrix

	服务降级
	服务熔断
	服务限流
	接近实时的监控
	
	http://localhost:8001/payment/hystrix/ok/2
	
	http://localhost:8001/payment/hystrix/timeout/2
	
	服务降级
	http://localhost/consumer/payment/hystrix/timeout/2
	-----PaymentFallbackService fall back-paymentInfo_TimeOut ,o(╥﹏╥)o
	
	```
	通用的
	@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
	 //降级标签
    @HystrixCommand
	
	
	// 下面是全局fallback方法
    public String payment_Global_FallbackMethod()
    {
        return "Global异常处理信息，请稍后再试，/(ㄒoㄒ)/~~";
    }
	
	```
	
	全局配置 
	在接口中写 
	
	@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
	
	PaymentFallbackService 实现接口完成 降级方法。
	
	http://localhost:80/consumer/payment/hystrix/ok/2
	
	Hystrix	服务降级 解偶 完成
	
#### 服务熔断
     
	 ```
	 //=====服务熔断
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"),// 失败率达到多少后跳闸
    })
	
	http://localhost:8001/payment/circuit/1
	
	
	
	
	```
	
	
#### 接近实时的监控


```
/**
     *此配置是为了服务监控而配置，与服务容错本身无关，springcloud升级后的坑
     *ServletRegistrationBean因为springboot的默认路径不是"/hystrix.stream"，
     *只要在自己的项目里配置上下面的servlet就可以了
     */
    @Bean
    public ServletRegistrationBean getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/hystrix.stream");
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }
	
	http://localhost:8001/hystrix.stream
```	


###  11.zuul 路由网关




###  12.Gateway 网关
	异步非阻赛模型
	
	三个概念：路由Route，断言predicate，过滤Filter。
	

	http://localhost:8001/payment/get/3

	Gateway
	http://localhost:9527/payment/get/7

	http://localhost:9527/payment/lb

#### 断言
  predicate 就是为了实现一组匹配规则，让请求过来找到对应的Route进行处理

#### 过滤Filter
    使用过滤器，可以在请求被路由前或是路由后对请求进行过滤。
	自定义过滤器
	http://localhost:9527/payment/lb?name=123
   
 
 
 ###  13.springcloud config 分布式配置中心
 
 创建gitee 有一个仓库
 文件config-dev.yml
 ```
 config:
  info: master branch, springcloud-config/config-dev.yml version=1
  
 ```
 cloud-config-client-3355 项目
 ```
 //对应的是gitee  config 文件中的yml 信息
   @Value("${config.info}")
    private String configInfo;

 
 ```
 
 http://localhost:3355/configInfo
 
 curl -X POST "http://localhost:3355/actuator/refresh"
 
 ```
 C:\Users\86189>curl -X POST "http://localhost:3355/actuator/refresh"
["config.client.version","config.info"]
 
 ```
 
 ###  14.springBus 消息总线
   RabbitMQ
   springCloud Bus
   安装 RabbitMQ  注意版本
   https://blog.csdn.net/qq_25919879/article/details/113055350
   guest 
   http://127.0.0.1:15672
   全部通知
   curl -X POST "http://localhost:3344/actuator/bus-refresh"
   
   config-client
   指定驱动
   curl -X POST "http://localhost:3344/actuator/bus-refresh/config-client:3355"
   
###  15.springCloud Stream 消息驱动 
   
    http://localhost:8801/sendMessage
   
   
###  16.springCloud Sleuth  分布式请求链路跟踪

Java -jar zipkin-server-2.12.9-exec.jar
http://127.0.0.1:9411/zipkin/

###  17.springCloud Alibaba 入门 


###  18.springCloud Alibaba Nacos 服务注册和配置中心 
注册中心 配置中心
http://192.168.23.1:8848/nacos


```
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://localhost:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=root


startup -m standalone

```

startup -m standalone

//IP 
http://192.168.28.12:8848/nacos



###  19.springCloud  Alibaba sentinel 实现熔断与限流 
 sentinel 
 https://blog.51cto.com/u_15072914/3897800
 java -jar sentinel-dashboard-1.8.3.jar
 
 > 下载好以后通过命令启动，默认8080端口，可以使用-Dserver.port=8888控制启动端口，默认用户和密码都是sentinel，

-Dsentinel.dashboard.auth.username=sentinel: 用于指定控制台的登录用户名为 sentinel；
-Dsentinel.dashboard.auth.password=123456: 用于指定控制台的登录密码为 123456；如果省略这两个参数，默认用户和密码均为 sentinel
-Dserver.servlet.session.timeout=7200: 用于指定 Spring Boot 服务端 session 的过期时间，如 7200 表示 7200 秒；60m 表示 60 分钟，默认为 30 分钟



_Sentinel系统规则


```
http://localhost:84/consumer/paymentSQL/1
http://localhost:8401/testA
```

```
[
    {
        "resource":"/rateLimit/byUrl",
        "limitApp":"default",
        "grade":1,
        "count":1,
        "strategy":0,
        "controlBehavior":0,
        "clusterMode":false
    }
]
```

http://localhost:8401/rateLimit/byUrl

###  20.springCloud Alibaba Seata 处理分布式式务


http://localhost:2001/order/create?userId=1&productId=1&count=1&money=2

--  库存
http://localhost:2002/storage/decrease?productId=1&count=1

-- 支付
http://localhost:2003/account/decrease?userId=1&money=2

