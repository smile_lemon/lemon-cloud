package com.lemon.springcloud.service.impl;

import com.lemon.springcloud.service.IMessageProvider;
import org.springframework.cloud.commons.util.IdUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @auther 柠檬
 * @create 2022-04-13  9:03
 **/
@EnableBinding(Source.class)//定义消息推送的管道
public class MessageProviderImpl implements IMessageProvider {

    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        String serialNumber = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serialNumber).build());
        System.out.println("**************************"+serialNumber);
        return null;
    }
}
