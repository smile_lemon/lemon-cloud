package com.lemon.springcloud.service;

/**
 * @auther 柠檬
 * @create 2022-04-13  9:02
 **/

public interface IMessageProvider {
    String send();
}
