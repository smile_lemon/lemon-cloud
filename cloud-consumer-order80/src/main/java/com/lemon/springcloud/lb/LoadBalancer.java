package com.lemon.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @auther 柠檬
 * @create 2022-04-09  11:24
 **/

public interface LoadBalancer {
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
