package com.lemon.springcloud.alibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.lemon.springcloud.alibaba.service.PaymentService;
import com.lemon.springcloud.entities.CommonResult;
import com.lemon.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 *
 */
@RestController
@Slf4j
public class OrderNacosController
{
//    @Resource
//    private RestTemplate restTemplate;
    @Resource
    private PaymentService paymentService;

//    @Value("${service-url.nacos-user-service}")
//    private String serverURL;

//    @GetMapping(value = "/consumer/payment/nacos/{id}")
//    public String paymentInfo(@PathVariable("id") Long id)
//    {
//        return restTemplate.getForObject(serverURL+"/payment/nacos/"+id,String.class);
//    }
//
//    @GetMapping(value = "/consumer/paymentSQL/{id}")
//    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id)
//    {
//
////        return paymentService.paymentSQL(id);
//        return new CommonResult<>(3333,"服务降级返回,---paymentSQL",new Payment(id,"paymentSQL"));
//    }
//
//
//    @GetMapping("/consumer/fallback/{id}")
//   // @SentinelResource(value = "fallback") //没有配置
//    @SentinelResource(value = "fallback",fallback = "handlerFallback") //fallback只负责业务异常
//    //@SentinelResource(value = "fallback",blockHandler = "blockHandler") //
//    // 当fallback和 blockHandler都配置了，只负责sentinel控制台配置违规  ,只会进入blockHandler
////    @SentinelResource(value = "fallback",fallback = "handlerFallback",blockHandler = "blockHandler",
////            exceptionsToIgnore = {IllegalArgumentException.class})
//    public CommonResult<Payment> fallback(@PathVariable Long id)
//    {
//        CommonResult<Payment> result = restTemplate.getForObject(serverURL + "/paymentSQL/"+id,CommonResult.class,id);
//
//        if (id == 4) {
//            throw new IllegalArgumentException ("IllegalArgumentException,非法参数异常....");
//        }else if (result.getData() == null) {
//            throw new NullPointerException ("NullPointerException,该ID没有对应记录,空指针异常");
//        }
//
//        return result;
//    }
//    //本例是fallback
//    public CommonResult handlerFallback(@PathVariable  Long id,Throwable e) {
//        Payment payment = new Payment(id,"null");
//        return new CommonResult<>(444,"兜底异常handlerFallback,exception内容  "+e.getMessage(),payment);
//    }
//    //本例是blockHandler
//    public CommonResult blockHandler(@PathVariable  Long id, BlockException blockException) {
//        Payment payment = new Payment(id,"null");
//        return new CommonResult<>(445,"blockHandler-sentinel限流,无此流水: blockException  "+blockException.getMessage(),payment);
//    }

   // ==================OpenFeign



    @GetMapping(value = "/consumer/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id)
    {
        return paymentService.paymentSQL(id);
    }

}
